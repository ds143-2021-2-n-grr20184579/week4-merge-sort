#include <stdio.h>
#include <stdlib.h>
#include "merge.h"

void printArray(int A[], int size)
{
	int i;
	for (i = 0; i < size; i++)
		printf("%d ", A[i]);
	printf("\n");
}

void insertion_sort(int *v, int inicio, int fim)
{
	long int i, j, aux, x;

	for (i = inicio; i <= fim; i++)
	{
		x = v[i];
		j = i;
		while (x < v[j - 1] && j > 0)
		{
			v[j] = v[j - 1];
			j--;
		}
		v[j] = x;
	}
}

void mergeSort(int *arr, int l, int r)
{
	if (l < r)
	{
		int m = l + (r - l) / 2;

		if (r - l <= 10)
		{
			printf("\nIsertion sort para %d elementos", r - l);
			insertion_sort(arr, l, r);
		}
		else
		{
			mergeSort(arr, l, m);
			mergeSort(arr, m + 1, r);
			merge(arr, l, m, r);
		}
	}
}

int main()
{
	int array[] = {964, 930, 467, 56, 697, 529, 652, 118, 259, 678, 158, 988, 172, 746, 893, 827, 342, 277, 123, 924, 221, 365, 820, 41, 920, 517, 772, 732, 592, 112, 554, 141, 286, 880, 898, 34, 770, 503, 104, 743, 937, 698, 250, 984, 615, 600, 762, 917, 73, 153, 883, 856, 634, 901, 583, 67, 230, 836, 178, 916, 18, 336, 907, 818, 147, 105, 20, 301, 315, 860, 529, 566, 803, 928, 10, 542, 947, 807, 666, 312, 964, 946, 475, 562, 935, 363, 864, 946, 141, 191, 712, 817, 865, 839};
	int array_size = sizeof(array) / sizeof(array[0]);

	printf("Array fornecido:\n");
	printArray(array, array_size);

	mergeSort(array, 0, array_size - 1);

	printf("\n\nArray ordenado:\n");
	printArray(array, array_size);

	return 0;
}
